$(document).ready(function() {

	// 
	// Resize the first heigh 'poster block'
	// to the full height of the window
	// 
	var posterFullHeightDecreaser = 40;

	$(window).resize(sizeContent);

	function sizeContent() {
	    var quoteHeight,
	    	// newWidthRatio = $(window).width() + "px",
	    	gamesListItemHalfMih = 480,
	    	gamesListItemHalf = $(".js-games-list_item-half"), // may be deleted
	    	gamesListItemFull = $(".js-games-list_item-full"), // may be deleted
	    	gamesListItemHalfHolder = $(".js-games-list_item-half-holder"), // may be deleted
	    	gamesListItemFullHolder = $(".js-games-list_item-full-holder"), // may be deleted
	    	newHeight = $(window).height() + "px",
	    	newHeightNotFull = $(window).height() - posterFullHeightDecreaser + "px",
	    	newHeightNotFullWithoutMenu = $(window).height() - posterFullHeightDecreaser - 100 + "px",
	    	newHeight60p = $(window).height()*0.6 + "px",
	    	newHeightHalf = $(window).height()*0.5 + "px",
	    	newHeightHalfMb = $(window).height()*0.5 - 1 + "px";

	    $(".js-poster-fullheight").css("height", newHeight);
	    $(".js-poster-nofullheight").css("height", newHeightNotFull);
	    $(".js-poster-height-60p").css("height", newHeight60p);
	    $(".js-poster-half-height").css("height", newHeightHalf);

	    $(".js-poster-half-height-desktop-tablet").css("height", newHeightHalf);
	    if ($(window).width() <= 767 ) {
	    	$(".js-poster-half-height-desktop-tablet").css("height", newHeightNotFullWithoutMenu);
	    }
	    if ($(window).width() <= 640 && $(window).height() < 480  ) {
	    	$(".js-poster-height-60p").css("height", newHeightNotFull);
	    }

	    if ( $(window).height()*0.5 > 380 && $(window).width() >= 400 ) {
            $(".js-poster-halfheight-bottom-padding").css("padding-bottom", newHeightHalfMb);
        }
         else {
        	if ($(window).width() < 400 ) {
            	$(".js-poster-halfheight-bottom-padding").css("padding-bottom", '460px');
			    $(".js-poster-half-height").css("height", '460px');
        	}
        }

        $(".js-top-center").each(function(){
        	$(this).css("margin-top", $(this).height()/2*(-1) + "px");
        });

        $(".js-horiz-center").each(function(){
        	$(this).css("margin-left", $(this).width()/2*(-1) + "px");
        });

  		// if ( !gamesListItemHalf.hasClass('games-list_item-half-mobile') && ($(window).width() <= (gamesListItemHalf.height()*2.05 + 60)) ) {
	   //      	gamesListItemHalf.addClass('games-list_item-half-mobile');
				// gamesListItemFull.addClass('games-list_item-full-mobile');
				// gamesListItemHalfHolder.addClass('games-list_item-half-holder-mobile');
				// gamesListItemFullHolder.addClass('games-list_item-full-holder-mobile');
  		// } else {
  		// 	if (
  		// 		gamesListItemHalf.hasClass('games-list_item-half-mobile')
  		// 		&& ($(window).width() > (gamesListItemHalfMih*2 + 60))
  		// 	) {
  		// 		gamesListItemHalf.removeClass('games-list_item-half-mobile');
				// gamesListItemFull.removeClass('games-list_item-full-mobile');
				// gamesListItemHalfHolder.removeClass('games-list_item-half-holder-mobile');
				// gamesListItemFullHolder.removeClass('games-list_item-full-holder-mobile');
  		// 	}
  		// }

	}

	sizeContent();
	window.setTimeout(sizeContent, 100);

	// 
	// 'Poster block' resize ends
	// 


   	
   	// 
   	// Carousel plugin slider initialization & configuration
   	// 
   	
	function carouFredSelInit (Slider, visible, responsive, width, height, autoplay, align, swipe) {

		if (typeof $().carouFredSel == 'function') {

			// Cache slide navigation
			Slider.each(function(i, iSlider) {
				
				iSlider = $(iSlider);

				var sliderContainer = iSlider.parents('.js-slider-container'),

					options = { // Common slider options
						// width: '100%',
						// height: 'auto',				
						width: width,
						height: height,
						items: {
							visible: visible,
							// height: '100%',
							width: 3000
						},
						align: align,
						responsive: responsive,
						// circular: false,
						scroll: {
							items: 1,
							fx: 'directscroll',
							duration: 600
						},
						auto: {
							play: autoplay,
							timeoutDuration: 7000
						},
						prev        : {
					        button      : sliderContainer.find(".js-prev"),
					        key 		: "left"
					    },
					    next        : {
					        button      : sliderContainer.find(".js-next"),
					        key 		: "right"
					    },
						swipe: {
						    onTouch: swipe,
						    onMouse: swipe
						},
						pagination: {
							container: sliderContainer.find('.js-slider-pager')
						},
					};

				iSlider.carouFredSel(options);
			});
		}
	}

	carouFredSelInit($('.quota-slider'), 1, true, 'auto', "100%", true, 'center', true);

	if (typeof $().carouFredSel == 'function') {

			// Cache slide navigation
		

		$('.front-slider').each(function(i, iSlider) {
				
			iSlider = $(iSlider);

			var sliderContainer = iSlider.parents('.js-slider-container'),

				options = { // Common slider options
					width: '100%',
					height: 'auto',				
					items: {
						visible: 1,
						width: 3000
					},
					align: 'center',
					responsive: true,
					scroll: {
						items: 1,
						fx: 'directscroll',
						duration: 600
					},
					synchronise: ['#front-images', false, true],
					auto: {
						play: true,
						timeoutDuration: 6000
					},
					prev        : {
				        button      : sliderContainer.find(".js-prev"),
				        key 		: "left"
				    },
				    next        : {
				        button      : sliderContainer.find(".js-next"),
				        key 		: "right"
				    },
					swipe: {
					    onTouch: true,
					    onMouse: true
					},
					pagination: {
						container: sliderContainer.find('.js-slider-pager')
					},
				};

			iSlider.carouFredSel(options);
		});

		$('#front-images').each(function(i, iSlider) {
			
			iSlider = $(iSlider);

			var sliderContainer = iSlider.parents('.js-slider-container'),

				options = { // Common slider options
					width: 'auto',
					height: '100%',
					items: {
						visible: 1,
						width: 3000
					},
					align: 'center',
					responsive: true,
					scroll: {
						items: 1,
						fx: 'directscroll',
						duration: 600
					},
					auto: {
						play: true,
						timeoutDuration: 6000
					}
				};

			iSlider.carouFredSel(options);
		});
	}

	// 
   	// End of Carousel plugin slider configuration
   	// 

	//		
	//		Main menu manipulations start
	//

	var previousScroll = 0, // previous scroll position
        menuOffset = 80, // height of menu (once scroll passed it, menu is hidden)
        detachPoint = $(window).height(), // point of detach (after scroll passed it, menu is fixed)
        hideShowOffset = 6; // scrolling value after which triggers hide/show menu

    // on scroll hide/show menu
    // 
    $(window).scroll(function() {

      if (!$('#main-nav').hasClass('expanded')) {
        var currentScroll = $(this).scrollTop(), // gets current scroll position
            scrollDifference = Math.abs(currentScroll - previousScroll); // calculates how fast user is scrolling

        // if scrolled past menu
        if (currentScroll > menuOffset) {
            // if scrolled past detach point add class to fix menu
            if (currentScroll > detachPoint) {
                if (!$('#main-nav').hasClass('detached')) {
                    $('#main-nav').addClass('detached');
                }
                // if scrolling faster than hideShowOffset hide/show menu
                if (scrollDifference >= hideShowOffset) {
                    if (currentScroll > previousScroll) {
                        $('#main-nav').addClass('invisible');
                    } else {
                        $('#main-nav').removeClass('invisible');
                    }
                }
            } else {
                $('#main-nav').removeClass();
            }

        /*
          if (currentScroll <= detachPoint){
          }
        */

        }
        // replace previous scroll position with new one
        previousScroll = currentScroll;
      }
    });


	$(window).bind('touchstart',function(){
		$('.has-layers').addClass('special-styles');
	}).bind('touchend',function(){
		$('.has-layers').removeClass('special-styles');
	});

    // shows/hides navigation’s popover if class "expanded"
    $('#main-nav .header-menu-nav-link').on('click touchstart', function(event) {
      showHideNav();
      event.preventDefault();
    });

    // clicking anywhere inside navigation or heading won’t close navigation’s popover
    $('#main-menu .header-menu-nav-link').on('click touchstart', function(event){
        event.stopPropagation();
    });

    // checks if navigation’s popover is shown
    function showHideNav() {
      if ($('#main-nav').hasClass('expanded')) {
        hideNav();
      } else {
        showNav();
      }
    }

    // shows the navigation’s popover
	var firstMove;
    
	$(document).ready(function() {
		window.addEventListener('touchstart', function (e) {
			if($('#main-nav').hasClass('expanded')) {
		    	firstMove = true;
			} else {
				firstMove = false;
			}
		});
		window.addEventListener('touchmove', function (e) {
		    if (firstMove) {
		        e.preventDefault();
		        firstMove = false;
		    }
		});
	})

    function showNav() {
		$('#main-nav').removeClass('invisible').addClass('expanded show');
		window.setTimeout(function(){
			$('body').addClass('no_scroll');
			$(window).resize();
		}, 1); // Firefox hack. Hides scrollbar as soon as menu animation is done
		$('#main-menu a').attr('tabindex', ''); // links inside navigation should be TAB selectable
    }

    // hides the navigation’s popover
    function hideNav() {
		// $('#container').removeClass('blurred');
		$('#main-nav').removeClass('show');   
		$('.header-menu-nav-link').blur();
		firstMove = false;
		window.setTimeout(function(){
			$('body').removeClass();
			$('#main-nav').removeClass('expanded');
			$(window).resize();
			

		}, 350); // allow animations to start before removing class (Firefox)
		$('#main-menu a').attr('tabindex', '-1'); // links inside hidden navigation should not be TAB selectable
		$('.dynamic-icon').blur(); // deselect icon when navigation is hidden
    }

    // keyboard shortcuts
    $('body').keydown(function(e) {
      // menu accessible via TAB as well
      if ($("#main-nav .dynamic-icon").is(":focus")) {
        // if ENTER/SPACE show/hide menu
        if (e.keyCode === 13 || e.keyCode === 32) {
          showHideNav();
          e.preventDefault();
        }
      }

      // if ESC show/hide menu
      if (e.keyCode === 27 || e.keyCode === 77) {
        showHideNav();
        e.preventDefault();
      }
    });


	$(function() {

		if ($('.watched-block').eq(0).length) {

		    $(window).on('scroll', function (argument) {
		        var $element = $('.watched-block').eq(0),
		            $relatedElement = $($element.attr('data-related-element')),
		            relatedElementClass = $element.attr('data-related-element-class'),
		            $bottomLastLayer = $('.js-bottom-last-layer').eq(0),
		            $bottomPreLastLayer = $('.js-bottom-prelast-layer'),
		            offsetLimit = $element.offset().top + $element.height(),
		            offsetLimitLast = $bottomPreLastLayer.offset().top + $bottomPreLastLayer.height() - 100,
		            windowScroll;
		       
		        if(!$element.hasClass('watched-block-full')) {
		            windowScroll = window.pageYOffset + $(window).height() - posterFullHeightDecreaser;
		        } else {
		        	if($element.hasClass('poster') && $(window).height() <= 760) {
		        		windowScroll = window.pageYOffset + 760;
		        	}
		        	else {
			            windowScroll = window.pageYOffset + $(window).height();
		        	}
		        }

		        if(windowScroll > offsetLimit) {
		            $relatedElement.addClass(relatedElementClass);
		        } else {		        	
		            $relatedElement.removeClass(relatedElementClass);
		        }

		        if(windowScroll > offsetLimitLast) {
		            $bottomLastLayer.addClass('show-bottom');
		        }
		        else {
		        	$bottomLastLayer.removeClass('show-bottom');       	
		        }

		        if ($(window).scrollTop() > 0) {
		        	$('body').addClass('scrolling');
		        } else {
		        	$('body').removeClass('scrolling');       	
		        }

		    });
		}
		
		if ($('.js-bottom-last-layer').eq(0).length) {

		    $(window).on('scroll', function (argument) {
		        var $bottomLastLayer = $('.js-bottom-last-layer').eq(0),
		            $bottomPreLastLayer = $('.js-bottom-prelast-layer'),
		            offsetLimitLast = $bottomPreLastLayer.offset().top + $bottomPreLastLayer.height() - 150,
		            windowScroll = window.pageYOffset + $(window).height();

		        if(windowScroll > offsetLimitLast) {
		            $bottomLastLayer.addClass('show-bottom');
		        }
		        else {
		        	$bottomLastLayer.removeClass('show-bottom');       	
		        }

		        if ($(window).scrollTop() > 0) {
		        	$('body').addClass('scrolling');
		        } else {
		        	$('body').removeClass('scrolling');       	
		        }

		    });
		}


		// if ($('.js-animated').length) {

		// 	$(window).on('scroll', function (argument) {
		// 		var $elAnimated = $('.js-animated');
		// 		if ($(window).scrollTop() > 0) {
		//         	$elAnimated.addClass('stopped');
		//         } else {
		//         	$elAnimated.removeClass('stopped');       	
		//         }
		// 	});
		// }

		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 3000);
					return false;
				}
			}
		});
	});

});