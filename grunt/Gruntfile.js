module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        watch: {
            configFiles: {
                files: [ 'Gruntfile.js' ],
            },

            less: {
                files: ['../assets/src/less/*.less'],
                tasks: ['less']
            },

            options: {
                spawn: false,
            },

            sprite: {
                files: ['../assets/img/png/*.png'],
                tasks: ['sprite'],
            },
        },

        less: {
            development: {
                options: {
                    paths: ["../assets/css"]
                },
                files: {"../assets/css/structure.css": "../assets/src/less/structure.less"}
            }
        },

        sprite:{
            all: {
                src: '../assets/img/png/*.png',
                dest: '../assets/img/sprite.png',
                imgPath: '../img/sprite.png',
                cssFormat: 'less',
                destCss: '../assets/src/less/_sprite.less',
                algorithm: 'binary-tree',
                padding: 10
            }
        },

        // sprite:{
        //     dist: {
        //         src: '../assets/img/png/*.png',
        //         destImg: '../assets/img/sprite.png',
        //         destCSS: '../assets/src/less/_sprite.less',
        //         imgPath: '../img/sprite.png',
        //         padding: 10,
        //         cssFormat: 'less',
        //         algorithm: 'binary-tree'
        //     }
        // },
        

        
        // datauri: {
        //   options: {
        //     varPrefix: 'svg-'
        //   },
        //   your_target: {
        //     files: {
        //       src: "../assets/img/*.svg",
        //       dest: "../assets/src/less/_datauri_variables.scss"
        //     }
        //   }
        // },
        // grunticon: {
        //     myIcons: {
        //         files: [{
        //             expand: true,
        //             cwd: '../assets/img/svg',
        //             src: ['*.svg', '*.png'],
        //             dest: "../assets/src/less"
        //         }]
        //     }
        // }

    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-datauri-variables');
    grunt.loadNpmTasks('grunt-spritesmith');
    // grunt.loadNpmTasks('grunt-grunticon');

    // Register the default tasks.
    grunt.registerTask('default', ['sprite', 'less','watch']);

};