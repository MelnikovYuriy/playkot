// var gulp = require('gulp'),
// 	watch = require('gulp-watch');

// gulp.task('default', function() {
  
// 	gulp.src('css/**/*.css')
// 		.pipe(watch('css/**/*.css'))
// 		.pipe(gulp.dest('./build/'));

// });

// var less = require('gulp-less');
// var path = require('path');

// gulp.task('less', function () {
//   gulp.src('./asstes/src/less/*.less')
//     .pipe(less({
//       paths: [ path.join(__dirname, 'less', 'includes') ]
//     }))
//     .pipe(gulp.dest('./asstes/css'));
// });

var gulp = require('gulp'),
	less = require('gulp-less'),
	watch = require('gulp-watch'),
	path = require('path');

gulp.task('less', function() {
    return gulp.src('./assets/src/less/structure.less')  // only compile the entry file
        .pipe(less({
          paths: ['./assets/src/less/']
        } ))
        .pipe(gulp.dest('./assets/css'))
});

gulp.task('watch', function() {
    gulp.watch('./assets/src/less/*.less', ['less']);  // Watch all the .less files, then run the less task
});

gulp.task('default', ['watch']); // Default will run the 'entry' watch task